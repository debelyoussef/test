using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class CoinsManager : SingleTon<CoinsManager>
{

    private float collectedCoins=0, totalCoins;

    private const string COIN_TAG = "Coin";
    [SerializeField]
    private Text collectedCointxt, totalCoinstxt;
    [SerializeField]
    private Image bar;
    void Start()
    {
        InitUI();
    }

    private void InitUI()
    {
        var coins = GameObject.FindGameObjectsWithTag(COIN_TAG);
        totalCoins = coins.Length;
        totalCoinstxt.text = totalCoins.ToString();
        bar.fillAmount = 0;
    }

    public void CollectCoin()
    { 
        collectedCoins++;
        collectedCointxt.text = collectedCoins.ToString();
        try
        {
            bar.fillAmount = collectedCoins / totalCoins;
        }
        catch (DivideByZeroException)
        {
            print("Divide by zero impossible , make sure your total coins number is more than 0");
        }
    }


}
