using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

 //   const string PLAYERNAME = "PLAYER";
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == PlayerMouvements.PLAYERNAME)
        {
            CoinsManager.instance.CollectCoin();
            Destroy(this.gameObject);
        }
    }

}
