using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMouvements : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    private CircleCollider2D collider;
    //layer mask for ground objects
    [SerializeField]
    private LayerMask groundLayer;
    //How Up the touch's target position should be to make the player jump ?
    [SerializeField]
    private float jumpheightOffset;
    //Do The player have to jump when reaches the target position
    private bool havejump=false;
    //the maximum distance between the collider and ground , for the player object to be considered grounded
    private const float groundCheckHeight = 0.15f;
    private Camera cam;
    [SerializeField]
    SpriteRenderer sprite;
    //The position the player wants to go to.
    private Vector3 targetPos;
    //Player's Speed
    [SerializeField]
    private float speed , maxSpeed , minSpeed;
    //Speed UI Text
    [SerializeField]
    private Text speedtxt;
    //minmum distance between player and target Pos
    private float minDistance = 0.15f;
    //current Velocity of rb (to make it go left or right)
    private Vector2 currentVelocity;
    //is current TargetPos reached ?
    private bool IsCurrentTargetPosreached = true;
    //is the player moving ?
    private bool isMoving = false;
    //Jump Force
    private Vector2 jumpForce = new Vector2(0, 250);
    [SerializeField]
    private Animator anim;
    //the rectTransform's bar
    [SerializeField]
    private RectTransform bottomBar;
    //the minimum worldSpace value vertical Y
    private float minWorldSpaceVertical;

    public const string PLAYERNAME = "PLAYER";
    void Awake()
    {
        gameObject.name = PLAYERNAME;
        cam = Camera.main;
        speedtxt.text = speed.ToString();
       
    }

    private void Start()
    {
        CalculateTheBottomBarArea();
    }


    // Update is called once per frame
    void Update()
    {
        ProcessUpdate();
    }
    private void ProcessUpdate()
    {

#if UNITY_ANDROID
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                var worldpos = cam.ScreenToWorldPoint(touch.position);
                SetTargetPos(worldpos);
            }
        }
#endif
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            var worldpos = cam.ScreenToWorldPoint(Input.mousePosition);
            SetTargetPos(worldpos);
        }
#endif
        }

    private void FixedUpdate()
    {
        ProcessFixedUpdate();
    }

    private void ProcessFixedUpdate()
    {
        
        if (IsCurrentTargetPosReached() && isMoving)
        {
            rb.velocity = new Vector2(0, 0);
            if (havejump)
                Jump();
            isMoving = false;
            anim.SetBool("isMoving", isMoving);
        }
        else if(isMoving)
        {
            rb.velocity = currentVelocity;
        }
    }

    private void SetTargetPos(Vector3 worldpos)
    {
            if (worldpos.y > minWorldSpaceVertical)
                targetPos = worldpos;
            else
                return;
            Vector3 direction = targetPos - transform.position;
            if (direction.x > 0)
            {
                sprite.flipX = false;
                currentVelocity = new Vector2(speed, rb.velocity.y);
            }
            else
            {
                sprite.flipX = true;
                currentVelocity = new Vector2(-speed, rb.velocity.y);
            }
            if (direction.y >= jumpheightOffset)
            {
                havejump = true;
            }     
            targetPos = new Vector3(targetPos.x, transform.position.y, transform.position.z);
            IsCurrentTargetPosreached = false;
            isMoving = true;
            anim.SetBool("isMoving", isMoving);
    }

    //Calculate the minimum bottom of the screen the player could click on to reach the place , if the player clicks on the bar bottom their click won't be considered
    private void CalculateTheBottomBarArea()
    {
        Vector3[] coordinates = new Vector3[4];
        bottomBar.GetWorldCorners(coordinates);
        minWorldSpaceVertical = cam.ScreenToWorldPoint(coordinates[1]).y;
    }
    private bool IsCurrentTargetPosReached()
    {
        if (IsCurrentTargetPosreached)
            return true;
        if (Vector3.Distance(transform.position, targetPos) <= minDistance)
        {
            IsCurrentTargetPosreached = true;
            return true;
        }       
        else
            return false;
    }

    private void Jump()
    {
       if(IsGrounded())
          rb.AddForce(jumpForce);     
        havejump = false;
     }

    private bool IsGrounded()
    {
        
        RaycastHit2D raycastHit = Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.down, groundCheckHeight, groundLayer);
        return raycastHit.collider != null;   
    }

    public void SetSpeed(float step)
    {
        if (step < 0)
        {
            speed = Mathf.Max(minSpeed, speed + step);
        }
        else
        {
            speed = Mathf.Min(maxSpeed, speed + step);
        }
        speedtxt.text = speed.ToString();
        anim.SetFloat("speed", speed);
        if (sprite.flipX)
            currentVelocity = new Vector2(-speed, rb.velocity.y);
        else
            currentVelocity = new Vector2(speed, rb.velocity.y);
    }
    

}
